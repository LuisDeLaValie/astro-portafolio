import React, { useEffect, useState } from 'react'
import './proyectos.css'

import type { Repositorios } from '../../models/repositorios';


interface Props {
    data: Repositorios[];
}

export const Proyectos = ({ data }: Props): React.JSX.Element => {
    const [proyectos, setProyectos] = useState<Repositorios[]>([]);

    useEffect(() => {

        setProyectos(data);

    }, []);


    return (
        <div className="grid grid-cols-2 md:grid-cols-4 gap-4">

            {proyectos.map(repo => (


                <figure className="relative max-w-sm transition-all duration-300 cursor-pointer filter grayscale hover:grayscale-0">
                    <a href={repo.svn_url}>
                        <img className="rounded-lg" src="/images/Logo-cuadrado.png" alt="image description" />
                    </a>
                    <figcaption className="absolute px-4 text-lg text-white font-semibold bottom-6">
                        <p> {repo.name} </p>
                    </figcaption>
                </figure>


            ))}

        </div>
    )
}

