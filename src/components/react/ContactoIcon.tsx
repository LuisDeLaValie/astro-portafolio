import React from 'react'

import './ContactoIcon.css'

interface Props {
    icon: string;
    enlace?: string;
    texto: string;
    copiar?: string;
    color?: string;
}

export const ContactoIcon = ({ icon, texto, enlace, copiar, color }: Props): React.JSX.Element => {
    const copy = (text: string) => {
        navigator.clipboard.writeText(text);
    }

    const open = (url: string) => {
        window.open(url);
    }

    return (
        <div
            className="DatosInfo m-2 p-2 rounded-full bg-slate-700 text-white overflow-hidden inline-block"
            style={{ backgroundColor: color }}
        >
            <div className="contenido flex flex-row items-center justify-center">
                <div className="smallInfo hover:text-gray-500 hover:cursor-pointer">
                    <span className="icon"> {icon}</span>
                </div>
                <div className="morInfo">
                    <label
                        className="textoInfo text-black bg-slate-200 py-2 px-1 mx-2 rounded-md cursor-text text-sm"
                    >
                        {texto}
                    </label>
                    <button
                        className="abrir-btn text-lg m-1 hover:cursor-pointer hover:text-cyan-600"
                        onClick={() => open(enlace)}
                    >󰖟</button>
                    <button
                        className="copiar-btn text-lg m-1 hover:cursor-pointer hover:text-cyan-600"
                        onClick={() => copy(copiar)}>󰆏</button>
                </div>
            </div>
        </div>);
}
