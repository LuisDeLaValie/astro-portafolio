export interface Repositorios {
    name: string;
    id: number;
    private: boolean;
    description: string;
    svn_url: string;
    created_at: string;
    updated_at: string;
    topics: string[];
}