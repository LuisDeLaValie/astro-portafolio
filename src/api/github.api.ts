import axios from "axios";

/* export const getRepositories = async () => {
    const axios = require('axios');
    let data = JSON.stringify({
        query: ``,
        variables: {}
    });

    let config = {
        method: 'get',
        maxBodyLength: Infinity,
        url: 'http://api.github.com/user/repos?sort=updated',
        headers: {
            'Accept': 'application/vnd.github.v3+json',
            'Authorization': 'Bearer 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',
            'Content-Type': 'application/json'
        },
        data: data
    };


    axios.request(config)
        .then((response) => {
            console.log(JSON.stringify(response.data));
        })
        .catch((error) => {
            console.log(error);
        });

} */

export const githubApi = axios.create({
    baseURL: "http://api.github.com/",
    timeout: 10000,
    headers: {
        'Accept': 'application/vnd.github.v3+json',
        'Authorization': 'Bearer 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',
        'Content-Type': 'application/json'
    },

})