import React, { useEffect, useRef, useState } from 'react';
import './skills.css'

interface Props {
  habilidades: string[];
  titulo: string;
}

const Skills = ({ habilidades, titulo }: Props) => {

  const [breackPoint, setBreackPoint] = useState(0);
  const refContainer = useRef<HTMLDivElement>();
  var audio: HTMLAudioElement;
  useEffect(() => {
    handleResize()
    audio = document.querySelector("audio");
    
    const figures = document.querySelectorAll(".hexagon");



    window.addEventListener("resize", handleResize);
  });

  const handleResize = () => {


    if (refContainer.current) {
      const w = refContainer?.current?.offsetWidth;
      const is = 112;
      const calcular = ((w / is) - 1).toFixed(0);

      const breakk = parseInt(calcular);
      setBreackPoint(breakk);

    }

  }

  var skills = habilidades.map(e => 'hexagon ' + e);
  skills = skills.map((e, i) => i % breackPoint == 0 && i % (breackPoint * 2) != 0 && i != 0 ? `${e} hexagon-salto` : e);
  return (
    <div className=''>
      <audio preload="auto">
        <source src="/138060085 (mp3cut.net).wav" type="audio/wav" />
      </audio>
      <h1> {titulo} </h1>
      <div
        ref={refContainer}
        className="hexagon-content">
        {
          skills.map((e, i) => (<span
            key={i}
            style={{ backgroundImage: "url('/Polygon 1.svg')" }}
            className={e}
            onMouseEnter={() => { audio.pause(); audio.play(); }}
          ></span>))
        }
      </div>

    </div>
  )
}

export default Skills