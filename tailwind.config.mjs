/** @type {import('tailwindcss').Config} */
import flowbitePlugin from 'flowbite/plugin';

export default {
	content: [
		'./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}',
		'./node_modules/flowbite/**/*.js'
	],
	theme: {
		extend: {
			colors: {
				transparent: 'transparent',
				current: 'currentColor',
				'primary': {
					50: "#e6e9ed",
					100: "#bec8d5",
					200: "#96a4b8",
					300: "#6f819b",
					400: "#506787",
					500: "#314f77",
					600: "#2a486f",
					700: "#223f64",
					DEFAULT: "#1d3557",
					900: "#17253e"
				},
				'secondary': {
					50: "#e4f5f9",
					100: "#bce6f0",
					200: "#95d6e7",
					300: "#77c5dd",
					400: "#66b9d8",
					500: "#5cadd2",
					600: "#549fc4",
					700: "#4a8cb1",
					DEFAULT: "#457b9d",
					900: "#385c7a"
				},
				'ternary': {
					50: "#feebf0",
					100: "#fdced7",
					200: "#ed9ba3",
					300: "#e3757f",
					400: "#ef5460",
					500: "#f54149",
					DEFAULT: "#e63947",
					700: "#d43040",
					800: "#c62939",
					900: "#b71e2e"
				}
			},
		},
	},
	plugins: [
		flowbitePlugin,
	],
};
